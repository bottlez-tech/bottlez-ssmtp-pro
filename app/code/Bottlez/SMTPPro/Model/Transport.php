<?php

/**
 * Class description
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */

namespace Bottlez\SMTPPro\Model;

class Transport extends \Zend_Mail_Transport_Smtp implements \Magento\Framework\Mail\TransportInterface {

    /**
     * @var \Magento\Framework\Mail\MessageInterface
     */
    protected $_message;
    /**
     * @var \Bottlez\SMTPPro\Model\LogsFactory
     */
    protected $_modelLogsFactory;
    /**
     * @param \Magento\Framework\Mail\MessageInterface $message
     * @param \Bottlez\SMTPPro\Helper\Data $dataHelper
     * @param \Bottlez\SMTPPro\Model\LogsFactory $modelLogsFactory
     * @throws \Zend_Mail_Exception
     */
    public function __construct(
        \Magento\Framework\Mail\MessageInterface $message,
        \Bottlez\SMTPPro\Helper\Data $dataHelper,
        \Bottlez\SMTPPro\Model\LogsFactory $modelLogsFactory
    ) {
        if (!$message instanceof \Zend_Mail) {
            throw new \InvalidArgumentException('The message should be an instance of \Zend_Mail');
        }
        //Set reply-to path
        $setReturnPath = $dataHelper->getConfigSetReturnPath();
        switch ($setReturnPath) {
            case 1:
                $returnPathEmail = $message->getFrom();
                break;
            case 2:
                $returnPathEmail = $dataHelper->getConfigReturnPathEmail();
                break;
            default:
                $returnPathEmail = null;
                break;
        }

        if ($returnPathEmail !== null && $dataHelper->getConfigSetReturnPath()) {
            $message->setReturnPath($returnPathEmail);
        }
        if ($message->getReplyTo() === NULL && $dataHelper->getConfigSetReplyTo()) {
            $message->setReplyTo($returnPathEmail);
        }

        //set config
        $smtpConf = [
            'auth' => strtolower($dataHelper->getConfigAuth()),
            'username' => $dataHelper->getConfigUsername(),
            'password' => $dataHelper->getConfigPassword(),
            'port'=> $dataHelper->getConfigPort(),
        ];

        if ($dataHelper->getConfigSsl() != 'none') {
            $smtpConf['ssl'] = $dataHelper->getConfigSsl();
        }

        $this->_modelLogsFactory = $modelLogsFactory;
        $smtpHost = $dataHelper->getConfigSmtpHost();
        parent::__construct($smtpHost, $smtpConf);
        $this->_message = $message;
    }
    /**
     * Send a mail using this transport
     *
     * @return void
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendMessage() {
        try {
            parent::send($this->_message);
            $mailData = [
                'to' => implode(',', $this->_message->getRecipients()),
                'template' => '',
                'subject' => $this->_message->getSubject(),
                'email_body' => $this->body
            ];

            $logs = $this->_modelLogsFactory->create();
            $logs->addData($mailData);
            $logs->save();
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\MailException(new \Magento\Framework\Phrase($e->getMessage()), $e);
        }
    }

}