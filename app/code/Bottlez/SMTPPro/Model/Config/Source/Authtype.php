<?php

/**
 * Type of auth
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\SMTPPro\Model\Config\Source;

class Authtype implements \Magento\Framework\Option\ArrayInterface {

    /**
     * @return array
     */
    public function toOptionArray() {
        return [
            ['value' => 'none', 'label' => 'No SSL'],
            ['value' => 'ssl',  'label' => 'SSL'],
            ['value' => 'tls',  'label' => 'TLS']
        ];
    }

}