<?php

/**
 * Class description
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\SMTPPro\Model\ResourceModel\Logs;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Bottlez\SMTPPro\Model\Logs', 'Bottlez\SMTPPro\Model\ResourceModel\Logs');
    }
}