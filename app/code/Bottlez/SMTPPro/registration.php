<?php
/**
 * Description
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Bottlez_SMTPPro',
    __DIR__
);