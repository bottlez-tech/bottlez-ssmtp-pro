<?php

/**
 * Class description
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */
namespace Bottlez\SMTPPro\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $_encryptor;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\ObjectManagerInterface
     * @return void
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    )
    {
        $this->_encryptor = \Magento\Framework\App\ObjectManager::getInstance()->get(
            'Magento\Framework\Encryption\EncryptorInterface'
        );
        parent::__construct($context);
    }

    /**
     * Get system config password
     *
     * @param \Magento\Store\Model\ScopeInterface::SCOPE_STORE $store
     * @return string
     */
    public function getConfigPassword($store_id = null){
        return $this->_encryptor->decrypt($this->scopeConfig->getValue('system/smtpproapp/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id));
    }

    /**
     * Get system config username
     *
     * @param \Magento\Store\Model\ScopeInterface::SCOPE_STORE $store
     * @return string
     */
    public function getConfigUsername($store_id = null){
        return $this->scopeConfig->getValue('system/smtpproapp/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id);
    }

    /**
     * Get system config password
     *
     * @param \Magento\Store\Model\ScopeInterface::SCOPE_STORE $store
     * @return string
     */
    public function getConfigAuth($store_id = null){
        return $this->scopeConfig->getValue('system/smtpproapp/auth', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id);
    }

    /**
     * Get system config ssl
     *
     * @param \Magento\Store\Model\ScopeInterface::SCOPE_STORE $store
     * @return string
     */
    public function getConfigSsl($store_id = null){
        return $this->scopeConfig->getValue('system/smtpproapp/ssl', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id);
    }

    /**
     * Get system config password
     *
     * @param \Magento\Store\Model\ScopeInterface::SCOPE_STORE $store
     * @return string
     */
    public function getConfigSmtpHost($store_id = null){
        return $this->scopeConfig->getValue('system/smtpproapp/smtphost', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id);
    }

    /**
     * Get system config reply to
     *
     * @param \Magento\Store\Model\ScopeInterface::SCOPE_STORE $store
     * @return bool
     */
    public function getConfigSetReplyTo($store_id = null){
        return $this->scopeConfig->getValue('system/smtpproapp/set_reply_to', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id);
    }



    /**
     * Get system config set return path
     *
     * @param \Magento\Store\Model\ScopeInterface::SCOPE_STORE $store
     * @return int
     */
    public function getConfigSetReturnPath($store_id = null){
        return $this->scopeConfig->getValue('system/smtpproapp/set_return_path', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id);
    }


    /**
     * Get system config return path email
     *
     * @param \Magento\Store\Model\ScopeInterface::SCOPE_STORE $store
     * @return string
     */
    public function getConfigReturnPathEmail($store_id = null){
        return $this->scopeConfig->getValue('system/gmailsmtpapp/return_path_email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id);
    }

    public function getConfigPort($store_id = null){
        return $this->scopeConfig->getValue('system/smtpproapp/port', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store_id);
    }
}