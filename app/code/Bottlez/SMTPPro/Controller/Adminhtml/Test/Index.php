<?php

/**
 * Class description
 *
 * @author Sintsov Roman <roman@bottlez.com>
 * @copyright Copyright (c) 2016, Bottlez LTD
 */

namespace Bottlez\SMTPPro\Controller\Adminhtml\Test;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;
use Bottlez\SMTPPro\Model\LogsFactory;

class Index extends Action {

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var LogsFactory
     */
    protected $_modelLogsFactory;

    /**
     * @var \Bottlez\SMTPPro\Helper\Data
     */
    protected $_dataHelper;
    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Bottlez\SMTPPro\Helper\Data $dataHelper
     * @param LogsFactory $modelLogsFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Bottlez\SMTPPro\Helper\Data $dataHelper,
        \Bottlez\SMTPPro\Model\LogsFactory $modelLogsFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_dataHelper = $dataHelper;
        $this->_modelLogsFactory = $modelLogsFactory;
        parent::__construct($context);
    }
    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute() {
        $request = $this->getRequest();
        $store_id = $request->getParam('store', null);

        $name = 'Bottlez SMTPPro App Test';
        $username = $request->getPost('username');
        //$password = $request->getPost('password');
        $password = $this->_dataHelper->getConfigPassword($store_id);

        //if default view
        //see https://github.com/magento/magento2/issues/3019
        if(!$request->getParam('store', false)){
            if(empty($username) || empty($password)){
                $this->getResponse()->setBody(__('Please enter a valid username/password'));
                return;
            }
        }

        $to = $request->getPost('email') ? $request->getPost('email') : $username;
        //SMTP server configuration
        $smtpHost = $request->getPost('smtphost');

        $smtpConf = array(
            'auth' => strtolower($request->getPost('auth')),
            'username' => $username,
            'password' => $password,
            'port' => $request->getPost('port')
        );

        $ssl = $request->getPost('ssl');
        if ($ssl != 'none') {
            $smtpConf['ssl'] = $ssl;
        }

        $transport = new \Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);
        $from = trim($request->getPost('from_email'));
        $from = \Zend_Validate::is($from, 'EmailAddress') ? $from : $username;

        //Create email
        $mail = new \Zend_Mail();
        $mail->setFrom($from, $name);
        $mail->addTo($to, $to);
        $mail->setSubject('Hello from SMTPro');
        $mail->setBodyText('Thank you for choosing Bottlez SMTPPro extension.');

        $result = __('Sent... Please check your email') . ' ' . $to;

        try {
            //only way to prevent zend from giving a error
            if (!$mail->send($transport) instanceof \Zend_Mail){}
            $mailData = [
                'to' => $to,
                'template' => null,
                'subject' => $mail->getSubject(),
                'email_body' => $mail->getBodyText()->getContent()
            ];

            $logs = $this->_modelLogsFactory->create();
            $logs->addData($mailData);
            $logs->save();
        } catch (\Exception $e) {
            $result = __($e->getMessage());
        }

        $this->getResponse()->setBody($this->makeClickableLinks($result));
    }

    /**
     * Make link clickable
     * @param string $s
     * @return string
     */
    public function makeClickableLinks($s) {
        return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $s);
    }
    /**
     * Is the user allowed to view the blog post grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Bottlez_SMTPPro');
    }

}